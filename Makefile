# Makefile for Project Afloat
.DEFAULT_GOAL := help

ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

# configure some directories that are relative to wherever ROOT_DIR is located
BUILD_DIR := $(ROOT_DIR)/build

# Ensure this isn't being run as root (not relevant for windows)
ifndef WINDOWS
  # You musn't ever run this as root
  ifeq ($(shell whoami),root)
    $(error You must not run this as root)
  endif
endif


# Clean out undesirable variables from the environment and command-line
# to remove the chance that they will cause problems with our build
define SANITIZE_VAR
$(if $(filter-out undefined,$(origin $(1))),
  $(info *NOTE*      Sanitized $(2) variable '$(1)' from $(origin $(1)))
  MAKEOVERRIDES = $(filter-out $(1)=%,$(MAKEOVERRIDES))
  override $(1) :=
  unexport $(1)
)
endef


# These specific variables can influence gcc in unexpected (and undesirable) ways
SANITIZE_GCC_VARS := TMPDIR GCC_EXEC_PREFIX COMPILER_PATH LIBRARY_PATH
SANITIZE_GCC_VARS += CFLAGS CPATH C_INCLUDE_PATH CPLUS_INCLUDE_PATH OBJC_INCLUDE_PATH DEPENDENCIES_OUTPUT
SANITIZE_GCC_VARS += ARCHFLAGS
$(foreach var, $(SANITIZE_GCC_VARS), $(eval $(call SANITIZE_VAR,$(var),disallowed)))


# Decide on a verbosity level based on the V= parameter
export AT := @

ifndef V
export V0    :=
export V1    := $(AT)
else ifeq ($(V), 0)
export V0    := $(AT)
export V1    := $(AT)
else ifeq ($(V), 1)
endif


# Populate list of all board targets.
ALL_BOARDS :=                                  # Start with an empty list
include $(ROOT_DIR)/firmware/*/target-info.mk  # Automatically populate the list

# Python setup
PYTHON	:= python
PYTEST	:= pytest

##############################
#
# Help instructions
#
##############################
.PHONY: help
help:
	@echo
	@echo "   This Makefile is known to work on Linux and Mac in a standard shell environment."
	@echo "   It might also work on Windows, although this is untested."
	@echo
	@echo "   Here is a summary of the available targets:"
	@echo "   [Big Hammer]"
	@echo "     all                  - Generate firmware, analytics, etc..."
	@echo "     all_fw               - Build only firmware for all boards"
	@echo
	@echo "     all_clean            - Remove your build directory ($(BUILD_DIR))"
	@echo "     all_fw_clean         - Remove firmware for all boards"
	@echo
	@echo "     all_<board>          - Build all available images for <board>"
	@echo "     all_<board>_clean    - Remove all available images for <board>"
	@echo
	@echo "     all_ut               - Build all unit tests"
	@echo "     all_ut_run           - Run all unit tests and dump output to console"
	@echo
	@echo "   [Firmware]"
	@echo "     <board>              - Build firmware for <board>"
	@echo "                            supported boards are ($(ALL_BOARDS))"
	@echo "     fw_<board>           - Build firmware for <board>"
	@echo "                            supported boards are ($(FW_BOARDS))"
	@echo "     fw_<board>_clean     - Remove firmware for <board>"
	@echo "     fw_<board>_program   - Use OpenOCD + SWD/JTAG to write firmware to <board>"
	@echo
	@echo "   [Unit tests]"
	@echo "     ut_<test>            - Build unit test <test>"
	@echo "     ut_<test>_run        - Run test and dump output to console"


##############################
#
# Generic build
#
##############################

# Define list of recipes to be targeted by `make all`
.PHONY: all
all: all_fw


##############################
#
# Hardware related components
#
##############################

# $(1) = Canonical board name all in lower case (e.g. nexus)
# $(2) = Platform
# $(3) = Board serial port
define FW_TEMPLATE
.PHONY: $(1) fw_$(1)
$(1): fw_$(1)_atfw
fw_$(1): fw_$(1)_atfw

fw_$(1)_atfw: TARGET=fw_$(1)
fw_$(1)_atfw: OUTDIR=$(BUILD_DIR)/$$(TARGET)/
fw_$(1)_atfw: FIRMWARE_ROOT_DIR=$(ROOT_DIR)/firmware/
fw_$(1)_atfw:
	$(V0) @echo " BUILD      $$@"
	$(V1) mkdir -p $$(OUTDIR)/
	$(V1) cd $$(FIRMWARE_ROOT_DIR) && \
		$$(MAKE) -r --no-print-directory \
		build \
		APP_TARGET=$(1) \
		PLATFORM=$(2) \
		TARGET_DIR=$$(OUTDIR) \

.PHONY: $(1)_program
$(1)_program: fw_$(1)_program
fw_$(1)_program: TARGET=fw_$(1)
fw_$(1)_program: OUTDIR=$(BUILD_DIR)/$$(TARGET)/
fw_$(1)_program: FIRMWARE_ROOT_DIR=$(ROOT_DIR)/firmware/
fw_$(1)_program:
	$(V0) @echo " PROGRAM      $$@"
	$(V1) mkdir -p $$(OUTDIR)/
	$(V1) cd $$(FIRMWARE_ROOT_DIR) && \
		$$(MAKE) -r --no-print-directory \
		flash \
		APP_TARGET=$(1) \
		PLATFORM=$(2) \
		PORT=$(3) \
		TARGET_DIR=$$(OUTDIR) \

.PHONY: $(1)_clean
$(1)_clean: fw_$(1)_clean
fw_$(1)_clean: TARGET=fw_$(1)
fw_$(1)_clean: OUTDIR=$(BUILD_DIR)/$$(TARGET)
fw_$(1)_clean:
	$(V0) @echo " CLEAN      $$@"
	$(V1) [ ! -d "$$(OUTDIR)" ] || $(RM) -r "$$(OUTDIR)"
endef


# Start out assuming that we'll build fw for all boards
FW_BOARDS  := $(ALL_BOARDS)

# Generate the targets for boards in the list
FW_TARGETS := $(addprefix fw_, $(FW_BOARDS))

.PHONY: all_fw all_fw_clean
all_fw:        $(addsuffix _atfw,  $(FW_TARGETS))
all_fw_clean:  $(addsuffix _clean, $(FW_TARGETS))

# Expand the firmware rules
$(foreach board, $(FW_BOARDS), $(eval $(call FW_TEMPLATE,$(board),$($(board)_platform),$($(board)_port))))


##############################
#
# Unit Tests
#
##############################

ALL_UNITTESTS :=
ALL_PYTHON_UNITTESTS := python_analytics_ut

UT_OUT_DIR := $(BUILD_DIR)/unit_tests

$(UT_OUT_DIR):
	$(V1) mkdir -p $@

.PHONY: all_ut
all_ut: $(addsuffix _elf, $(addprefix ut_, $(ALL_UNITTESTS))) $(ALL_PYTHON_UNITTESTS)

.PHONY: all_ut_run
all_ut_run: $(addsuffix _run, $(addprefix ut_, $(ALL_UNITTESTS))) $(ALL_PYTHON_UNITTESTS)

.PHONY: all_ut_clean
all_ut_clean:
	$(V0) @echo " CLEAN      $@"
	$(V1) [ ! -d "$(UT_OUT_DIR)" ] || $(RM) -r "$(UT_OUT_DIR)"


# $(1) = Unit test name
define UT_TEMPLATE
.PHONY: ut_$(1)
ut_$(1): ut_$(1)_run

ut_$(1)_%: TARGET=$(1)
ut_$(1)_%: OUTDIR=$(UT_OUT_DIR)/$$(TARGET)
ut_$(1)_%: UT_ROOT_DIR=$(ROOT_DIR)/flight/tests/$(1)
ut_$(1)_%: $$(UT_OUT_DIR)
	$(V1) mkdir -p $(UT_OUT_DIR)/$(1)
	$(V1) cd $$(UT_ROOT_DIR) && \
		$$(MAKE) -r --no-print-directory \
		BUILD_TYPE=ut

.PHONY: ut_$(1)_clean
ut_$(1)_clean: TARGET=$(1)
ut_$(1)_clean: OUTDIR=$(UT_OUT_DIR)/$$(TARGET)
ut_$(1)_clean:
	$(V0) @echo " CLEAN      $(1)"
	$(V1) [ ! -d "$$(OUTDIR)" ] || $(RM) -r "$$(OUTDIR)"
endef

# Expand the unittest rules
$(foreach ut, $(ALL_UNITTESTS), $(eval $(call UT_TEMPLATE,$(ut))))

.PHONY: python_analytics_ut
python_analytics_ut:
	$(V0) @echo "  PYTHON_ANALYTICS_UT"
	$(V1) $(PYTEST) analytics/tests
