# Add this board to the list of buildable boards
ALL_BOARDS += nexus

# Set the board here that matches your Particle
# Should be one of: xenon,argon,boron
nexus_platform := xenon

nexus_port := /dev/cu.usbmodem1421201
