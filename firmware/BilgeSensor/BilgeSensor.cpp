/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "Libraries/mesh_ack.h"

#include "gitcommit.h"

#include "BilgeSensor.h"


BilgeSensor::BilgeSensor(String sensorName, String dataFieldName, String description, std::initializer_list<pin_t> wakeUpPins, std::initializer_list<InterruptMode> edgeTriggerModes, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks) :
   AfloatAbstractNode(sensorName, dataFieldName, description, wakeUpPins, edgeTriggerModes, connectionInterval_s, maxNumPendingAcks)
{
   /*
    * Perform any node-specific initialization routines.
    * .
    * .
    * .
    */
}


void BilgeSensor::identityHandler(const char *event, const char *jsonMsgPacket)
{
   Serial.printlnf("[MESH][RX] event=%s, payload=%s", event, jsonMsgPacket ? jsonMsgPacket : "NULL");

   // Perform generic message processing
   bool ret = identityMsgProcessor(event, jsonMsgPacket);

   /* If the generic processor couldn't handle the message, then maybe it requires specialized
    * handling by the specific node.
    */
   if (ret == false) {
      /*
       * Perform any extra message handling here.
       * .
       * .
       * .
       */
   }
}


void BilgeSensor::publishData(time_t measurementTime, float cycleCount, uint32_t numberOfRetries)
{
   /* Prepare the JSON object. Please note that the JSON can have any number
    * of data elements and types in it. For different Afloat nodes, expand it as
    * as appropriate.
    */
   const size_t capacity = JSON_OBJECT_SIZE(2);
   DynamicJsonDocument sensorPacket(capacity);  // TODO: Change to static JSON document type
   sensorPacket["time"] = measurementTime;
   sensorPacket["cycleCount"] = cycleCount;

   // Console spew
   Serial.print("\t\t");
   serializeJson(sensorPacket, Serial);
   Serial.println("");

   /* Prepare the payload tuple. Please note that the tuple can have any number
    * of data elements and types in it. For different Afloat nodes, expand it as
    * as appropriate.
    */
   payload_t payloadData = payload_t(measurementTime, cycleCount);

   // Send the JSON object, and at the same time provide additional support data
   sendData(sensorPacket, payloadData, numberOfRetries);
}

void BilgeSensor::republishData(const payload_t &payloadData, uint32_t numberOfRetries)
{
   // Extract data from tuple
   int cycleCount_old;
   time_t measurementTime_old;
   std::tie(measurementTime_old, cycleCount_old) = payloadData;

   // Resend
   publishData(measurementTime_old, cycleCount_old, numberOfRetries);

//   Serial.println(String("[Mesh] Resending: ") + String(lastTransmitTime) + String("/") + String("(") + String(cycleTime_old) + String(",") +
//                  String(cycleCount_old) + ") timed out after " + String(ellapsedTime) + " s, resending.");
}
