# Add this board to the list of buildable boards
ALL_BOARDS += bilgesensor

# Set the board here that matches your Particle
# Should be one of: xenon,argon,boron
bilgesensor_platform := xenon

bilgesensor_port := /dev/cu.usbmodem1421101
