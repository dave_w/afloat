/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "mesh_ack.h"

//! Local macros

//! Local defines
#define BUFFER_DEPTH 10

//! Local variables
CircularBufferWithForgetting circBuf(BUFFER_DEPTH);
static uint8_t publishMsgCount = 0;


//! Local functions


/**
 * @brief CircularBufferWithForgetting::CircularBufferWithForgetting Constructor
 * @param maxNumberOfElements
 */
CircularBufferWithForgetting::CircularBufferWithForgetting(uint32_t numBufferRows)
{
   this->numBufferRows = numBufferRows;
   data = (int16_t *)malloc(numBufferRows);
}


/**
 * @brief CircularBufferWithForgetting::~CircularBufferWithForgetting Destructor. Important to
 *        release the memory assigned to the Flexible Array Member
 */
CircularBufferWithForgetting::~CircularBufferWithForgetting()
{
   free(data);
}


/**
 * @brief CircularBufferWithForgetting::remove Remove the ID from the list
 * @param ID
 */
int32_t CircularBufferWithForgetting::remove(int32_t ID)
{
   uint32_t tmpTail = head;

   // Loop through the entire circular buffer
   do {
      // Check if we've found the ID yet
      if (data[tmpTail] == ID) {
         // Clear the data
         data[tmpTail] = -1;

         // Decrement the count
         countStillActive--;

         // END
         return ID;
      }

      // Increment tmpTail
      tmpTail++;

      // Wrap tmpTail if necessary
      if (tmpTail >= numBufferRows) {
         tmpTail = 0;
      }
   } while(tmpTail != head);

   Serial.println("[ACK][WARNING] Item 0x" + String(ID, HEX) + " not found. head: " + String(head) + ", tail: " + String(head));

   return -1;
}


/**
 * @brief CircularBufferWithForgetting::compactBuffer Removes `-1` from the
 *        buffer, and shifts up the remaining data.
 */
void CircularBufferWithForgetting::compactBuffer()
{
   // Get tail
   uint32_t tmpTail = (head-1 + numBufferRows) % numBufferRows;
   uint32_t tmpCount = 0;

   for (uint32_t i=0; i< numBufferRows; i++) {
      uint32_t idxOfInterest = (head - (i + 1) + numBufferRows) % numBufferRows;

      if (data[idxOfInterest] >= 0) {
         // In the beginning, the indices are the same. So only write once
         // they've diverged.
         if (tmpTail != idxOfInterest) {
            data[tmpTail] = data[idxOfInterest];
         }

         // Wrap around if necessary. Be careful of the unsigned integer wrap-around where (0 - 1) --> 0xFFFF
         if (tmpTail == 0) {
            tmpTail = numBufferRows-1;
         } else {
            tmpTail--;
         }

         tmpCount++;
      }
   }

   /* Set instance variables */
   countStillActive = tmpCount;

   tail = tmpTail+1;
   // Wrap around if necessary
   if (tail >= 0) {
      tail = numBufferRows-1;
   }
}


/**
 * @brief CircularBufferWithForgetting::dequeue Removes the last element, as well
 *        as any other elements which are `-1`.
 */
void CircularBufferWithForgetting::dequeue()
{
   // Loop across all deleted data
   do
   {
      // Increment tail index
      tail++;

      // Decrement the number of elements still active
      countStillActive--;

      // Wrap around if necessary
      if (tail >= numBufferRows) {
         tail = 0;
      }
   } while(data[tail] == -1 && countStillActive > 0);  // If the stored data is `-1`, then this represents a deleted data point
}


/**
 * @brief CircularBufferWithForgetting::enqueue Add new data to the end of the circular buffer
 * @param newVal Value to be added to the buffer
 */
void CircularBufferWithForgetting::enqueue(int newVal)
{
   // Check if there's room. If not, try compacting the buffer to free up space.
   if (countStillActive == numBufferRows) {
      compactBuffer();
   }

   // Check again if there's room. If not, dequeue the oldest data.
   if (countStillActive == numBufferRows) {
      dequeue();
   }

   // Store data
   data[head] = newVal;

   // Increment head index
   head++;

   // Wrap around if necessary
   if (head >= numBufferRows) {
      head = 0;
   }

   // Increment the number of elements still active
   countStillActive++;
}



/**
 * @brief publishWithAck Publish this message to the clout and request an ACK
 * @param name Message topic
 * @param packet Message payload
 * @param packetID Packet ID (used for tracking ACK)
 * @return Returns the result from Particle' Mesh.publish() command
 */
int32_t publishWithAck(const char *name, MeshPacketWithAck &packet, uint8_t *packetID)
{
   // Write the packet ID
   packet.packet->ID = publishMsgCount;

   // Publish the hybrid data object
   int32_t ret = Mesh.publish(name, (const char *) packet.packet);

   // On successful transmit (0), send this to the awaiting ack buffer
   if (ret == 0) {
      circBuf.enqueue(packet.packet->ID);
      publishMsgCount++;
   }

   if (packetID != nullptr) {
      *packetID = packet.packet->ID;
   }

   return ret;
}


/**
 * @brief receiveAck
 * @param ID The ID to remove from the buffer
 * @return ID if successful, -1 otherwise
 */
int32_t receiveAck(int32_t ackedID)
{
   int32_t ret = circBuf.remove(ackedID);

   /* Serial.println("Received ack! " + String(ID) + "/" + String(ret) + ", buffer filled: " + circBuf.countStillActive); */

   return ret;
}


/**
 * @brief haltAck Gives up on trying to ACK a particular message
 * @param ID to be removed
 * @return ID if the ID was still tracked, -1 otherwise
 */
int32_t haltAck(int32_t ID)
{
   int32_t ret = circBuf.remove(ID);

   // Console error message
   Serial.println("[MESH][WARNING] Canceling ack! " + String(ID) + "/" + String(ret) + ", buffer filled: " + circBuf.countStillActive);

   return ret;
}
