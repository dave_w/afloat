/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "Libraries/mesh_ack.h"

#include "gitcommit.h"

#include "HoldingTank.h"


HoldingTank::HoldingTank(String sensorName, String dataFieldName, String description, uint32_t sampleInterval_ms, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks) :
   AfloatAbstractNode(sensorName, dataFieldName, description, sampleInterval_ms, connectionInterval_s, maxNumPendingAcks)
{
   /*
    * Perform any node-specific initialization routines.
    * .
    * .
    * .
    */
}


void HoldingTank::identityHandler(const char *event, const char *jsonMsgPacket)
{
   Serial.printlnf("[MESH][RX] event=%s, payload=%s", event, jsonMsgPacket ? jsonMsgPacket : "NULL");

   // Perform generic message processing
   bool ret = identityMsgProcessor(event, jsonMsgPacket);

   /* If the generic processor couldn't handle the message, then maybe it requires specialized
    * handling by the specific node.
    */
   if (ret == false) {
      /*
       * Perform any extra message handling here.
       * .
       * .
       * .
       */
   }
}


void HoldingTank::publishData(time_t measurementTime, float tankMeasurement_L, uint32_t numberOfRetries)
{
   /* Prepare the JSON object. Please note that the JSON can have any number
    * of data elements and types in it. For different Afloat nodes, expand it as
    * as appropriate.
    */
   const size_t capacity = JSON_OBJECT_SIZE(2);
   DynamicJsonDocument sensorPacket(capacity);  // TODO: Change to static JSON document type
   sensorPacket["time"] = measurementTime;
   sensorPacket["level_L"] = tankMeasurement_L;

   // Console spew
   Serial.print("\t\t");
   serializeJson(sensorPacket, Serial);
   Serial.println("");

   /* Prepare the payload tuple. Please note that the tuple can have any number
    * of data elements and types in it. For different Afloat nodes, expand it as
    * as appropriate.
    */
   payload_t payloadData = payload_t(measurementTime, tankMeasurement_L);

   // Send the JSON object, and at the same time provide additional support data
   sendData(sensorPacket, payloadData, numberOfRetries);
}

void HoldingTank::republishData(const payload_t &payloadData, uint32_t numberOfRetries)
{
   // Extract data from tuple
   int tankMeasurement_L_old;
   time_t measurementTime_old;
   std::tie(measurementTime_old, tankMeasurement_L_old) = payloadData;

   // Resend
   publishData(measurementTime_old, tankMeasurement_L_old, numberOfRetries);

//   Serial.println(String("[Mesh] Resending: ") + String(lastTransmitTime) + String("/") + String("(") + String(cycleTime_old) + String(",") +
//                  String(cycleCount_old) + ") timed out after " + String(ellapsedTime) + " s, resending.");
}
