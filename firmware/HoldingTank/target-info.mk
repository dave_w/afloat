# Add this board to the list of buildable boards
ALL_BOARDS += holdingtank

# Set the board here that matches your Particle
# Should be one of: xenon,argon,boron
holdingtank_platform := xenon

holdingtank_port = /dev/cu.usbmodem1421201
