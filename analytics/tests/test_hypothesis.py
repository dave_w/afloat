# -*- coding: utf-8 -*-
# pytest positive & negative controls

# No error (negative control) is default
# To induce errors, use either:
#   pytest --fail
#   python test_controls.py --fail

from hypothesis import given, settings, strategies as st
import sys
import pytest as pt 

# simple hypothesis example
MIN_POS_FLOAT = 1
MAX_POS_FLOAT = 1000
MAX_EXAMPLES = 5000

# Define hypothesis input data generation strategies per 
# https://hypothesis.readthedocs.io/en/latest/data.html

# Define a strategy to generate integers within a fixed range
pos_float_strategy = st.integers(min_value=MIN_POS_FLOAT, max_value=MAX_POS_FLOAT)

# Define a strategy to generate lists of integers, of a range of lengths
list_strategy = st.lists(pos_float_strategy, min_size=1, max_size=100, unique=True)

######################################################################
# following tests all FAIL. Run with: pytest --fail

# The python decorators @settings and @given come from hypothesis, and
# parametrize the test. See
# https://hypothesis.readthedocs.io/en/latest/details.html for more.
@settings(max_examples=MAX_EXAMPLES)
@given(x_vals=list_strategy)
def test_hypothesis_pass(fail, x_vals):
    if fail:
        # emulate obscure bug, fails when list is descending and all
        # elements are even integers
        if (len(x_vals) >= 2 and
            all ([xi >= xj for xi, xj in zip (x_vals, x_vals[1:])]) and
            all([x % 2 == 0 for x in x_vals])):
            assert False

if __name__ == '__main__':
    pt.main(sys.argv)
