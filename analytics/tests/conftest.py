# content of conftest.py

import pytest

def pytest_addoption(parser):
    parser.addoption(
        "--fail", action="store_true", default=False, help="Include tests that fail"
    )


@pytest.fixture
def fail(request):
    return request.config.getoption("--fail")
