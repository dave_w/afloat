
# Contributing

When contributing to this repository, please first discuss the change you wish to make. This keeps us all rowing in the same direction. 

## Pull Request Process

1. Ensure that the feature is as atomic as possible. It's 10 times easier to do 10 reviews of 10 lines of code than 1 review of 100.
2. Update the documentation when appropriate. In particular, new features are automatically considered orphaned if they're not somehow documented somewhere.
3. If you need to update any versioning, we use [SemVer](http://semver.org/).
4. Submitters should commit their own code. I.e., code should be reviewed and committed by someone other than the author.
 
## Code licensing

Contributors agree that the code can be relicensed if they give their permission. Permission will be asked by email, using the contributor's email address as logged by git. Contributors agree that relicensing permission is automatically granted if they do not respond within 180 days.