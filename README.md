
#  Afloat


Sailboat  sensor  suite



Afloat  generates  and  analyzes  data  to  help  you  sleep  better,  and  to  help  you  sail  better.



This  project  uses  mesh  sensors  to  send  data  up  to  the  cloud.  Once  in  the  cloud,  the  data  can  be  analyzed  with  various  algorithms.



The  project  directory  is  organized  into  the  following  sub-projects:

- firmware: the code which runs on the embedded hardware
- analytics: code and algorithms to perform batch and real-time analytics
- cloud: the configuration scripts to configure and run a data lake as well as visualization dashboard. Currently Afloat leverages the InfluxDB time-series database with Grafana dashboard toolkit.
- documentation: let's be honest, at this stage this directory is purely aspirational.
